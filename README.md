# IOSamuel

### [Reto YouTube Code](https://www.youtube.com/watch?v=GrhyWDUNCqM)

## Reto 1
![Reto](reto.png)


### Result
![Result](result.png)


## Tiempo
![Time](time.png)


## Reto 2
![Reto 2](reto-2.png)

#### Result
Para este era muy sencillo, no se midió el tiempo, pero fueron menos de 10 minutos.
Solo era una cruz, se rota y se tenía como tal.

![Result](result-r2.png)

Made with ♥ by [Jose Florez](www.joseflorez.co)
